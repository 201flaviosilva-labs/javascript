import Assets from "../../Assets.js";

const canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
const canvasWidth = 800;
const canvasHeight = 600;
canvas.width = canvasWidth;
canvas.height = canvasHeight;

let animationName = "Walk";

let images = {
	Dead: Assets.Sprites.Dino.Dead,
	Idle: Assets.Sprites.Dino.Idle,
	Jump: Assets.Sprites.Dino.Jump,
	Run: Assets.Sprites.Dino.Run,
	Walk: Assets.Sprites.Dino.Walk,
};

Object.keys(images).forEach(key => {
	for (let j = 0; j < images[key].length; j++) {
		const srcImage = images[key][j];
		images[key][j] = new Image();
		images[key][j].src = srcImage;
	}
});

let animationNumber = 0;
let actualIMG = images[animationName][animationNumber];

setInterval(() => {
	ctx.clearRect(0, 0, canvasWidth, canvasHeight);
	const x = canvasWidth / 2 - actualIMG.width / 2;
	const y = canvasHeight / 2 - actualIMG.height / 2;
	ctx.drawImage(actualIMG,
		0, 0, canvasWidth, canvasHeight,
		x, y, canvasWidth, canvasHeight);
}, 0);

setInterval(() => {
	animationNumber++;
	if (animationNumber >= images[animationName].length) animationNumber = 0;
	actualIMG = images[animationName][animationNumber];
}, 90);

const animationSelect = document.getElementById("animation");
animationSelect.addEventListener("change", () => {
	animationName = animationSelect.value;
});

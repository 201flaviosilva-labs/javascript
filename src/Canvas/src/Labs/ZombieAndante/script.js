import Assets from "../../Assets.js";

const canvas = document.getElementById("Canvas");
const context = canvas.getContext("2d");
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
const canvasWidth = canvas.width;
const canvasHeight = canvas.height;

const jogadorWidth = 65;
const jogadorHeight = 63;
let jogadorX = canvasWidth / 2 + jogadorWidth / 2;
let jogadorY = canvasHeight / 2 + jogadorHeight / 2;
const velocidade = 10;

let sprite = new Image();
sprite.src = Assets.Sprites.Zombie.zombie1;
sprite.addEventListener("load", desenhar);

const frameWidth = jogadorWidth;
const frameHeight = jogadorHeight;
let currentFrame = 0;

function desenhar() {
	context.clearRect(0, 0, canvasWidth, canvasHeight);
	context.drawImage(sprite, 0, currentFrame * frameHeight,
		frameWidth, frameHeight, jogadorX, jogadorY, frameWidth, frameHeight);
}

document.addEventListener("keydown", (e) => moverJogador(e));
function moverJogador(e) {
	const keyCode = e.keyCode;
	if (keyCode === 37 && jogadorX >= 0 + velocidade) {
		currentFrame = 2;
		jogadorX -= velocidade;
	}

	if (keyCode === 39 && jogadorX <= canvasWidth - jogadorWidth - velocidade) {
		currentFrame = 1;
		jogadorX += velocidade;
	}

	if (keyCode === 38 && jogadorY >= 0 + velocidade) {
		currentFrame = 0;
		jogadorY -= velocidade;
	}

	if (keyCode === 40 && jogadorY <= canvasHeight - jogadorHeight - velocidade) {
		currentFrame = 3;
		jogadorY += velocidade;
	}
	desenhar();
}

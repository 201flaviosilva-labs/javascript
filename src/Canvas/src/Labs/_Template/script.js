import { randomRGBColor, degreesToRadians } from "../../util.js";
import Assets from "../../Assets.js";

class _Template {
	constructor() {
		this.setup();
		this.init();
		this.create();
		this.update();
	}

	setup() {
		this.width = 800;
		this.height = 800;

		this.centerWidth = this.width / 2;
		this.centerHeight = this.height / 2;

		const canvas = document.createElement("canvas");
		this.ctx = canvas.getContext("2d");

		canvas.width = this.width;
		canvas.height = this.height;

		document.body.appendChild(canvas);
	}

	init() {
		console.log("Init");
	}

	create() {
		console.log("Create");
	}

	update() {
		console.log("Update");

		requestAnimationFrame(this.update.bind(this));
	}
}

const _template = new _Template();

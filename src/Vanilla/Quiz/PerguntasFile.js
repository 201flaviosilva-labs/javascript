export let perguntas = [{
        pergunta: "Quantos dentes tem um humano saudável?",
        resposta1: "5",
        resposta2: "32",
        resposta3: "50",
        resposta4: "100",
        solucao: "B",
        dificuldade: "MF",
        categoria: "Saude"
    },
    {
        pergunta: "Quantos ossos tem um humano saudável?",
        resposta1: "10563",
        resposta2: "5",
        resposta3: "0",
        resposta4: "206",
        solucao: "D",
        dificuldade: "MF",
        categoria: "Saude"
    },
    {
        pergunta: "Qual é o maior orgão do corpo humano?",
        resposta1: "Pele",
        resposta2: "Pé",
        resposta3: "Olho",
        resposta4: "Cabelo",
        solucao: "A",
        dificuldade: "MF",
        categoria: "Saude"
    },
    {
        pergunta: "Qual é o maior sistema do corpo humano?",
        resposta1: "Sistema Tegumentar (Pele)",
        resposta2: "Sistema Digestivo",
        resposta3: "Sistema Reprodutor",
        resposta4: "Sistema Esquelético",
        solucao: "A",
        dificuldade: "MF",
        categoria: "Saude"
    },
    {
        pergunta: "Qual o orgão mais pesado do corpo humano?",
        resposta1: "Pulmão",
        resposta2: "Pele",
        resposta3: "Coração",
        resposta4: "Cerebro",
        solucao: "B",
        dificuldade: "MF",
        categoria: "Saude"
    },
    {
        pergunta: "Quantos litros de sangue uma pessoa saudavel tem no corpo?",
        resposta1: "60L",
        resposta2: "100L",
        resposta3: "Média 5L",
        resposta4: "0L",
        solucao: "C",
        dificuldade: "MF",
        categoria: "Saude"
    },
    {
        pergunta: "Qual a mior ortéria do corpo?",
        resposta1: "Veia Esquerda",
        resposta2: "Veia Direita",
        resposta3: "Arteria Esquerda",
        resposta4: "Aorta",
        solucao: "D",
        dificuldade: "MF",
        categoria: "Saude"
    },
    {
        pergunta: "Qual desdes é um prato tipico português?",
        resposta1: "Sushi",
        resposta2: "Lasanha",
        resposta3: "Francesinha",
        resposta4: "Pizza",
        solucao: "C",
        dificuldade: "MF",
        categoria: "Cultura"
    }
];
function shuffle(array) { return array.sort(() => Math.random() - 0.5); }
function allEqual(array) { return array.every(v => v === array[0]); };

const BOARD_TEMPLATE = ["y", "y", "b", "b", "g", "g"];

const cards = Array.from(document.querySelectorAll(".board .card"));
const scoreUI = document.getElementById("score");

let board = shuffle(BOARD_TEMPLATE);
let selectedCards = [];
let score = 0;

reset();
document.getElementById("reset").addEventListener("click", reset);
function reset() {
	cards.forEach((c, i) => {
		c.disabled = false;
		c.classList.remove(getClass(i), "disabled", "selected");
		if (!c.hasAttribute("clickEventAdded")) c.addEventListener("click", () => flipCard(c, i)); // Check if the event already existe and add it

		c.setAttribute("data-id", i);
		c.setAttribute("clickEventAdded", true);
	});

	board = shuffle(BOARD_TEMPLATE);
}

function flipCard(c, i) {
	c.classList.add(getClass(i));
	selectedCards.push(c);
	c.classList.add("selected");
	if (selectedCards.length >= 2) {
		cards.forEach(c => c.disabled = true); // Disable cards
		setTimeout(evaluateSelected, 500);
	}
}

function evaluateSelected() {
	if (allEqual(selectedCards.map(c => c.classList.value))) {
		selectedCards.forEach(c => c.classList.add("disabled"));
		score++;
		scoreUI.innerHTML = score;
	}

	cards.filter(c => !c.classList.contains("disabled")).forEach(c => {
		const id = c.getAttribute("data-id");
		c.classList.remove(getClass(id));
		c.classList.remove("selected");
		c.disabled = false;
	});

	selectedCards = [];
}

function getClass(index) {
	switch (board[index]) {
		case "y": return "yellow";
		case "b": return "blue";
		case "g": return "green";
	}
}

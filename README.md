# JavaScript

## Description

Simples projetos em JavaScript e algumas Libs/Frameworks

## Preview
- [Wiki](https://bitbucket.org/201flaviosilva-labs/javascript/wiki/Home)

## Links
- [Play](https://master.d9sttv3hc1nun.amplifyapp.com);
- [Code](https://bitbucket.org/201flaviosilva-labs/javascript/);

## Docs
- [W3Schools](https://www.w3schools.com/js/);
- [MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript);
